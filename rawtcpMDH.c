/*
    Raw TCP packets
    Silver Moon (m00n.silv3r@gmail.com)
*/
#include <stdio.h> //for printf
#include <string.h> //memset
#include <sys/socket.h>    //for socket ofcourse
#include <stdlib.h> //for exit(0);
#include <errno.h> //For errno - the error number
#include <netinet/tcp.h>   //Provides declarations for tcp header
#include <netinet/ip.h>    //Provides declarations for ip header
#include "md5.h"
/* 
    96 bit (12 bytes) pseudo header needed for tcp header checksum calculation 
*/
#define TRANSPORT_MODE 0
#define TUNNEL_MODE 1
struct pseudo_header 
{
    u_int32_t source_address;
    u_int32_t dest_address;
    u_int8_t placeholder;
    u_int8_t protocol;
    u_int16_t tcp_length;
};
struct ip_header_AH
{

    unsigned char      iph_ihl;
    unsigned char      iph_ver;
    unsigned char      iph_tos;
    unsigned short int iph_len;
    unsigned short int iph_ident;
    unsigned char      iph_flags;
    unsigned short int iph_offset;
    unsigned char      iph_ttl;
    unsigned char      iph_protocol;
    unsigned short int iph_chksum;
    unsigned int       iph_sourceip;
    unsigned int       iph_destip;

};

 struct ah_header{

unsigned short int ah_protocal;
unsigned short int ahl;//lenhth
unsigned short int ah_reserved;
unsigned int ah_spi;
unsigned int ah_sn;
unsigned char ah_ad[32];
};
/*
    Generic checksum calculation function
*/
unsigned short csum(unsigned short *ptr,int nbytes) 
{
    register long sum;
    unsigned short oddbyte;
    register short answer;
 
    sum=0;
    while(nbytes>1) {
        sum+=*ptr++;
        nbytes-=2;
    }
    if(nbytes==1) {
        oddbyte=0;
        *((u_char*)&oddbyte)=*(u_char*)ptr;
        sum+=oddbyte;
    }
 
    sum = (sum>>16)+(sum & 0xffff);
    sum = sum + (sum>>16);
    answer=(short)~sum;
     
    return(answer);
}
char* ConvertToString(char *b)
{

  char outStr[256];
  sprintf(outStr,"%i",b);
  char *xp= outStr;
  return xp;
}
 


static void Transform ();

static unsigned char PADDING[64] =
{
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/* F, G and H are basic MD5 functions: selection, majority, parity */
#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & (~z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | (~z)))

/* ROTATE_LEFT rotates x left n bits */
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))

/* FF, GG, HH, and II transformations for rounds 1, 2, 3, and 4 */
/* Rotation is separate from addition to prevent recomputation */
#define FF(a, b, c, d, x, s, ac) \
  {(a) += F ((b), (c), (d)) + (x) + (UINT4)(ac); \
   (a) = ROTATE_LEFT ((a), (s)); \
   (a) += (b); \
  }
#define GG(a, b, c, d, x, s, ac) \
  {(a) += G ((b), (c), (d)) + (x) + (UINT4)(ac); \
   (a) = ROTATE_LEFT ((a), (s)); \
   (a) += (b); \
  }
#define HH(a, b, c, d, x, s, ac) \
  {(a) += H ((b), (c), (d)) + (x) + (UINT4)(ac); \
   (a) = ROTATE_LEFT ((a), (s)); \
   (a) += (b); \
  }
#define II(a, b, c, d, x, s, ac) \
  {(a) += I ((b), (c), (d)) + (x) + (UINT4)(ac); \
   (a) = ROTATE_LEFT ((a), (s)); \
   (a) += (b); \
  }

void MD5Init (mdContext)
MD5_CTX *mdContext;
{
    mdContext->i[0] = mdContext->i[1] = (UINT4)0;

    mdContext->buf[0] = (UINT4)0x67452301;
    mdContext->buf[1] = (UINT4)0xefcdab89;
    mdContext->buf[2] = (UINT4)0x98badcfe;
    mdContext->buf[3] = (UINT4)0x10325476;
}

void MD5Update (mdContext, inBuf, inLen)
MD5_CTX *mdContext;
unsigned char *inBuf;
unsigned int inLen;
{
    UINT4 in[16];
    int mdi;
    unsigned int i, ii;


    mdi = (int)((mdContext->i[0] >> 3) & 0x3F);

    /* update number of bits */
    if ((mdContext->i[0] + ((UINT4)inLen << 3)) < mdContext->i[0])
        mdContext->i[1]++;
    mdContext->i[0] += ((UINT4)inLen << 3);
    mdContext->i[1] += ((UINT4)inLen >> 29);

    while (inLen--)
    {
        /* add new character to buffer, increment mdi */
        mdContext->in[mdi++] = *inBuf++;

        /* transform if necessary */
        if (mdi == 0x40)
        {
            for (i = 0, ii = 0; i < 16; i++, ii += 4)
                in[i] = (((UINT4)mdContext->in[ii+3]) << 24) |
                        (((UINT4)mdContext->in[ii+2]) << 16) |
                        (((UINT4)mdContext->in[ii+1]) << 8) |
                        ((UINT4)mdContext->in[ii]);
            Transform (mdContext->buf, in);
            mdi = 0;
        }
    }
}

void MD5Final (mdContext)
MD5_CTX *mdContext;
{
    UINT4 in[16];
    int mdi;
    unsigned int i, ii;
    unsigned int padLen;

    in[14] = mdContext->i[0];
    in[15] = mdContext->i[1];


    mdi = (int)((mdContext->i[0] >> 3) & 0x3F);


    padLen = (mdi < 56) ? (56 - mdi) : (120 - mdi);
    MD5Update (mdContext, PADDING, padLen);


    for (i = 0, ii = 0; i < 14; i++, ii += 4)
        in[i] = (((UINT4)mdContext->in[ii+3]) << 24) |
                (((UINT4)mdContext->in[ii+2]) << 16) |
                (((UINT4)mdContext->in[ii+1]) << 8) |
                ((UINT4)mdContext->in[ii]);
    Transform (mdContext->buf, in);


    for (i = 0, ii = 0; i < 4; i++, ii += 4)
    {
        mdContext->digest[ii] = (unsigned char)(mdContext->buf[i] & 0xFF);
        mdContext->digest[ii+1] =
            (unsigned char)((mdContext->buf[i] >> 8) & 0xFF);
        mdContext->digest[ii+2] =
            (unsigned char)((mdContext->buf[i] >> 16) & 0xFF);
        mdContext->digest[ii+3] =
            (unsigned char)((mdContext->buf[i] >> 24) & 0xFF);
    }
}

static void Transform (buf, in)
UINT4 *buf;
UINT4 *in;
{
    UINT4 a = buf[0], b = buf[1], c = buf[2], d = buf[3];

    /* Round 1 */
#define S11 7
#define S12 12
#define S13 17
#define S14 22
    FF ( a, b, c, d, in[ 0], S11, 3614090360); /* 1 */
    FF ( d, a, b, c, in[ 1], S12, 3905402710); /* 2 */
    FF ( c, d, a, b, in[ 2], S13,  606105819); /* 3 */
    FF ( b, c, d, a, in[ 3], S14, 3250441966); /* 4 */
    FF ( a, b, c, d, in[ 4], S11, 4118548399); /* 5 */
    FF ( d, a, b, c, in[ 5], S12, 1200080426); /* 6 */
    FF ( c, d, a, b, in[ 6], S13, 2821735955); /* 7 */
    FF ( b, c, d, a, in[ 7], S14, 4249261313); /* 8 */
    FF ( a, b, c, d, in[ 8], S11, 1770035416); /* 9 */
    FF ( d, a, b, c, in[ 9], S12, 2336552879); /* 10 */
    FF ( c, d, a, b, in[10], S13, 4294925233); /* 11 */
    FF ( b, c, d, a, in[11], S14, 2304563134); /* 12 */
    FF ( a, b, c, d, in[12], S11, 1804603682); /* 13 */
    FF ( d, a, b, c, in[13], S12, 4254626195); /* 14 */
    FF ( c, d, a, b, in[14], S13, 2792965006); /* 15 */
    FF ( b, c, d, a, in[15], S14, 1236535329); /* 16 */

    /* Round 2 */
#define S21 5
#define S22 9
#define S23 14
#define S24 20
    GG ( a, b, c, d, in[ 1], S21, 4129170786); /* 17 */
    GG ( d, a, b, c, in[ 6], S22, 3225465664); /* 18 */
    GG ( c, d, a, b, in[11], S23,  643717713); /* 19 */
    GG ( b, c, d, a, in[ 0], S24, 3921069994); /* 20 */
    GG ( a, b, c, d, in[ 5], S21, 3593408605); /* 21 */
    GG ( d, a, b, c, in[10], S22,   38016083); /* 22 */
    GG ( c, d, a, b, in[15], S23, 3634488961); /* 23 */
    GG ( b, c, d, a, in[ 4], S24, 3889429448); /* 24 */
    GG ( a, b, c, d, in[ 9], S21,  568446438); /* 25 */
    GG ( d, a, b, c, in[14], S22, 3275163606); /* 26 */
    GG ( c, d, a, b, in[ 3], S23, 4107603335); /* 27 */
    GG ( b, c, d, a, in[ 8], S24, 1163531501); /* 28 */
    GG ( a, b, c, d, in[13], S21, 2850285829); /* 29 */
    GG ( d, a, b, c, in[ 2], S22, 4243563512); /* 30 */
    GG ( c, d, a, b, in[ 7], S23, 1735328473); /* 31 */
    GG ( b, c, d, a, in[12], S24, 2368359562); /* 32 */

    /* Round 3 */
#define S31 4
#define S32 11
#define S33 16
#define S34 23
    HH ( a, b, c, d, in[ 5], S31, 4294588738); /* 33 */
    HH ( d, a, b, c, in[ 8], S32, 2272392833); /* 34 */
    HH ( c, d, a, b, in[11], S33, 1839030562); /* 35 */
    HH ( b, c, d, a, in[14], S34, 4259657740); /* 36 */
    HH ( a, b, c, d, in[ 1], S31, 2763975236); /* 37 */
    HH ( d, a, b, c, in[ 4], S32, 1272893353); /* 38 */
    HH ( c, d, a, b, in[ 7], S33, 4139469664); /* 39 */
    HH ( b, c, d, a, in[10], S34, 3200236656); /* 40 */
    HH ( a, b, c, d, in[13], S31,  681279174); /* 41 */
    HH ( d, a, b, c, in[ 0], S32, 3936430074); /* 42 */
    HH ( c, d, a, b, in[ 3], S33, 3572445317); /* 43 */
    HH ( b, c, d, a, in[ 6], S34,   76029189); /* 44 */
    HH ( a, b, c, d, in[ 9], S31, 3654602809); /* 45 */
    HH ( d, a, b, c, in[12], S32, 3873151461); /* 46 */
    HH ( c, d, a, b, in[15], S33,  530742520); /* 47 */
    HH ( b, c, d, a, in[ 2], S34, 3299628645); /* 48 */

    /* Round 4 */
#define S41 6
#define S42 10
#define S43 15
#define S44 21
    II ( a, b, c, d, in[ 0], S41, 4096336452); /* 49 */
    II ( d, a, b, c, in[ 7], S42, 1126891415); /* 50 */
    II ( c, d, a, b, in[14], S43, 2878612391); /* 51 */
    II ( b, c, d, a, in[ 5], S44, 4237533241); /* 52 */
    II ( a, b, c, d, in[12], S41, 1700485571); /* 53 */
    II ( d, a, b, c, in[ 3], S42, 2399980690); /* 54 */
    II ( c, d, a, b, in[10], S43, 4293915773); /* 55 */
    II ( b, c, d, a, in[ 1], S44, 2240044497); /* 56 */
    II ( a, b, c, d, in[ 8], S41, 1873313359); /* 57 */
    II ( d, a, b, c, in[15], S42, 4264355552); /* 58 */
    II ( c, d, a, b, in[ 6], S43, 2734768916); /* 59 */
    II ( b, c, d, a, in[13], S44, 1309151649); /* 60 */
    II ( a, b, c, d, in[ 4], S41, 4149444226); /* 61 */
    II ( d, a, b, c, in[11], S42, 3174756917); /* 62 */
    II ( c, d, a, b, in[ 2], S43,  718787259); /* 63 */
    II ( b, c, d, a, in[ 9], S44, 3951481745); /* 64 */

    buf[0] += a;
    buf[1] += b;
    buf[2] += c;
    buf[3] += d;
}


void MDPrint (mdContext,inStr,buf)
MD5_CTX *mdContext;
char  *inStr;
char *buf;
{
    int i;
    char mdh5str[3];
    char outStr[32];
    mdh5str[3]='\0'; 
    for (i = 0; i < 16; i++){
        sprintf (inStr,"%02x", mdContext->digest[i]);
        sprintf(mdh5str,"%s",inStr);
        strcat(outStr,mdh5str);
    }
    for(i=0;i<32;i++)
    {
        buf[i]=outStr[i];
    }
    buf[32]='\0';

}

/* size of test block */
#define TEST_BLOCK_SIZE 1000

/* number of blocks to process */
#define TEST_BLOCKS 10000

/* number of test bytes = TEST_BLOCK_SIZE * TEST_BLOCKS */
static long TEST_BYTES = (long)TEST_BLOCK_SIZE * (long)TEST_BLOCKS;

#define RADIX 20
#define LEN_SPEC_SIMVOL 2



void MDString (inString,outString)
char *inString;
char *outString;
{

    //char outString[32];
   // int a=strlen(inString);
    MD5_CTX mdContext;
    char buf[32];
    MD5Init (&mdContext);
    MD5Update (&mdContext, inString, strlen(inString));
    MD5Final (&mdContext);
    MDPrint (&mdContext,outString,buf);
    
   // outStringMD[36]='\0';
    for(int i=0;i<32;i++)
    {
        outString[i]=buf[i];
    }
   // outString=buf;
    outString[32]='\0';
   // printf("%s\n", buf);
    //free(outStringMD);
}

void Start_Transport_Mode(datagram,source_ip, data , pseudogram)
char *datagram;
char *source_ip;
char *data;
char *pseudogram;
{
    int s = socket (PF_INET, SOCK_RAW, IPPROTO_TCP);
    int mode;
     mode = 0;
    if(s == -1)
    {
        //socket creation failed, may be because of non-root privileges
        perror("Failed to create socket");
        exit(1);
    }
    struct ip_header_AH *iphah=(struct ip_header_AH *) datagram;
 
    //AH header
    struct ah_header *ahh=(struct ah_header *) (datagram+ sizeof(struct ip_header_AH));  
         //IP header
    struct iphdr *iph = (struct iphdr *) (datagram+sizeof(struct ip_header_AH)+ sizeof (struct ah_header));
    //TCP header
    struct tcphdr *tcph = (struct tcphdr *) (datagram + sizeof (struct ip_header_AH) + sizeof(struct ah_header) + sizeof(struct iphdr));
    struct sockaddr_in sin;
    struct pseudo_header psh;
     
    //Data part
    data = datagram + sizeof(struct iphdr) + sizeof(struct ah_header) + sizeof(struct tcphdr)+sizeof(struct ip_header_AH);
    strcpy(data , "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
     
    //some address resolution
    strcpy(source_ip , "192.168.1.2");
    sin.sin_family = AF_INET;
    sin.sin_port = htons(8888);
    sin.sin_addr.s_addr = inet_addr ("127.0.0.8");
     //Fill in the IP Header_AH
    iphah->iph_ver=4;
    iphah->iph_ihl=5;
    iphah->iph_tos=0;
    iphah->iph_len=sizeof (struct iphdr) + sizeof(struct ah_header)+sizeof(struct ip_header_AH)+sizeof (struct tcphdr)+ strlen(data);
    iphah->iph_ident=htonl(53322);
    iphah->iph_offset=0;
    iphah->iph_ttl=200;
    iphah->iph_chksum=0;
    iphah->iph_protocol=51;
    iphah->iph_sourceip=inet_addr ( source_ip );
    iphah->iph_destip=sin.sin_addr.s_addr;
  
    //Fill in the IP Header
    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = sizeof (struct iphdr) + sizeof (struct tcphdr)+ strlen(data);
    iph->id = htonl (54321); //Id of this packet
    iph->frag_off = 0;
    iph->ttl = 257;
    iph->protocol = 6;
    iph->check = 0;      //Set to 0 before calculating checksum
    iph->saddr = inet_addr ( source_ip );    //Spoof the source ip address
    iph->daddr = sin.sin_addr.s_addr;
  
    //Ip checksum
    iph->check = csum ((unsigned short *) datagram, iph->tot_len);
    
    //AH Header
    ahh->ah_reserved=0;
    ahh->ahl=5;
    ahh->ah_protocal=6;
    ahh-> ah_spi=23;
    ahh-> ah_sn=22;
   
     
    //TCP Header
    tcph->source = htons (1234);
    tcph->dest = htons (80);
    tcph->seq = 0;
    tcph->ack_seq = 0;
    tcph->doff = 5;  //tcp header size
    tcph->fin=0;
    tcph->syn=1;
    tcph->rst=0;
    tcph->psh=0;
    tcph->ack=0;
    tcph->urg=0;
    tcph->window = htons (5840); /* maximum allowed window size */
    tcph->check = 0; //leave checksum 0 now, filled later by pseudo header
    tcph->urg_ptr = 0;
     



    //Now the TCP checksum
    psh.source_address = inet_addr( source_ip );
    psh.dest_address = sin.sin_addr.s_addr;
    psh.placeholder = 0;
    psh.protocol = IPPROTO_TCP;
    psh.tcp_length = htons(sizeof(struct tcphdr) + strlen(data) );
     
    int psize = sizeof(struct pseudo_header) + sizeof(struct tcphdr) + strlen(data);
    pseudogram = malloc(psize);
     
    memcpy(pseudogram , (char*) &psh , sizeof (struct pseudo_header));
    memcpy(pseudogram + sizeof(struct pseudo_header) , tcph , sizeof(struct tcphdr) + strlen(data));
     
    tcph->check = csum( (unsigned short*) pseudogram , psize);
     
    char strMd[512];
    char *strIP_Protocol;

    strIP_Protocol= ConvertToString(iph->protocol);
    strcat(strMd,strIP_Protocol);

    char *strIP_Ihl= ConvertToString(iph->ihl);
    strcat(strMd,strIP_Ihl);
    char *strIP_Ver=ConvertToString(iph->version);
    strcat(strMd,strIP_Ver);
    char *strIP_Len=ConvertToString(iph->tot_len );
    strcat(strMd,strIP_Len);
    char *strIP_ID= ConvertToString( iph->id );
     strcat(strMd,strIP_ID);
    char *strIP_DstIP=ConvertToString(iph->daddr);
     strcat(strMd,strIP_DstIP);

    char *strIP_SrcIP=ConvertToString(iph->saddr );
      strcat(strMd,strIP_SrcIP);
  
     char *strAH_Prot=ConvertToString(ahh->ah_protocal); 
    strcat(strMd,strAH_Prot);
    char *strAH_Reserverd= ConvertToString(ahh->ah_reserved);
      strcat(strMd,strAH_Reserverd);
    char *strAH_SPI= ConvertToString(ahh->ah_sn);
      strcat(strMd,strAH_SPI);
    char *strAH_Len=ConvertToString(ahh->ahl);
      strcat(strMd,strAH_Len);

    MDString(strMd,ahh->ah_ad);
    printf("%s\n",ahh->ah_ad );          




    //IP_HDRINCL to tell the kernel that headers are included in the packet
    int one = 1;
    const int *val = &one;
     
    if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0)
    {
        perror("Error setting IP_HDRINCL");
        exit(0);
    }
     
   
    int y=0;
   while (1)
    {
        //Send the packet
        if (sendto (s, datagram, iphah->iph_len ,  0, (struct sockaddr *) &sin, sizeof (sin)) < 0)
        {
            perror("sendto failed");
        }
        //Data send successfully
        else
        {
            printf ("Packet Send. Length : %d  FLOOD POWER!!!!\n" , iphah->iph_len);
           // printf("%s\n",ah_h );
        }
       // y=y+1;
    } 
};

 //void Gets
//}

int main (void)
{
     MD5_CTX mdContext;
    //Create a raw socket
    int s = socket (PF_INET, SOCK_RAW, IPPROTO_TCP);
    int mode;
     mode = 0;
    if(s == -1)
    {
        //socket creation failed, may be because of non-root privileges
        perror("Failed to create socket");
        exit(1);
    }
 
    //Datagram to represent the packet
    char datagram[4096] , source_ip[32] , *data , *pseudogram;
  
    //zero out the packet buffer
    memset (datagram, 0, 4096);
     if (mode==TRANSPORT_MODE)
     {


     }
     Start_Transport_Mode(datagram,source_ip, data , pseudogram);
   /*//IP header
    struct iphdr *iph = (struct iphdr *) datagram;
    //AH header
    struct ah_header *ahh=(struct ah_header *) (datagram+ sizeof(struct iphdr));  
    //TCP header
    struct tcphdr *tcph = (struct tcphdr *) (datagram + sizeof (struct iphdr) + sizeof(struct ah_header));
    struct sockaddr_in sin;
    struct pseudo_header psh;
     
    //Data part
    data = datagram + sizeof(struct iphdr) + sizeof(struct ah_header) + sizeof(struct tcphdr);
    strcpy(data , "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
     
    //some address resolution
    strcpy(source_ip , "192.168.1.2");
    sin.sin_family = AF_INET;
    sin.sin_port = htons(8888);
    sin.sin_addr.s_addr = inet_addr ("127.0.0.8");
     
    //Fill in the IP Header
    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = sizeof (struct iphdr) + sizeof (struct tcphdr) + sizeof(struct ah_header)+ strlen(data);
    iph->id = htonl (54321); //Id of this packet
    iph->frag_off = 0;
    iph->ttl = 255;
    iph->protocol = 51;
    iph->check = 0;      //Set to 0 before calculating checksum
    iph->saddr = inet_addr ( source_ip );    //Spoof the source ip address
    iph->daddr = sin.sin_addr.s_addr;
  
    //Ip checksum
    iph->check = csum ((unsigned short *) datagram, iph->tot_len);
    
    //AH Header
    ahh->ah_reserved=0;
    ahh->ahl=5;
    ahh->ah_protocal=6;
    ahh-> ah_spi=23;
    ahh-> ah_sn=22;
   
     
    //TCP Header
    tcph->source = htons (1234);
    tcph->dest = htons (80);
    tcph->seq = 0;
    tcph->ack_seq = 0;
    tcph->doff = 5;  //tcp header size
    tcph->fin=0;
    tcph->syn=1;
    tcph->rst=0;
    tcph->psh=0;
    tcph->ack=0;
    tcph->urg=0;
    tcph->window = htons (5840); maximum allowed window size 
    tcph->check = 0; //leave checksum 0 now, filled later by pseudo header
    tcph->urg_ptr = 0;
     



    //Now the TCP checksum
    psh.source_address = inet_addr( source_ip );
    psh.dest_address = sin.sin_addr.s_addr;
    psh.placeholder = 0;
    psh.protocol = IPPROTO_TCP;
    psh.tcp_length = htons(sizeof(struct tcphdr) + strlen(data) );
     
    int psize = sizeof(struct pseudo_header) + sizeof(struct tcphdr) + strlen(data);
    pseudogram = malloc(psize);
     
    memcpy(pseudogram , (char*) &psh , sizeof (struct pseudo_header));
    memcpy(pseudogram + sizeof(struct pseudo_header) , tcph , sizeof(struct tcphdr) + strlen(data));
     
    tcph->check = csum( (unsigned short*) pseudogram , psize);
     
    char strMd[512];
    char *strIP_Protocol;

    strIP_Protocol= ConvertToString(iph->protocol);
    strcat(strMd,strIP_Protocol);

    char *strIP_Ihl= ConvertToString(iph->ihl);
    strcat(strMd,strIP_Ihl);
    char *strIP_Ver=ConvertToString(iph->version);
    strcat(strMd,strIP_Ver);
    char *strIP_Len=ConvertToString(iph->tot_len );
    strcat(strMd,strIP_Len);
    char *strIP_ID= ConvertToString( iph->id );
     strcat(strMd,strIP_ID);
    char *strIP_DstIP=ConvertToString(iph->daddr);
     strcat(strMd,strIP_DstIP);

    char *strIP_SrcIP=ConvertToString(iph->saddr );
      strcat(strMd,strIP_SrcIP);
  
     char *strAH_Prot=ConvertToString(ahh->ah_protocal); 
    strcat(strMd,strAH_Prot);
    char *strAH_Reserverd= ConvertToString(ahh->ah_reserved);
      strcat(strMd,strAH_Reserverd);
    char *strAH_SPI= ConvertToString(ahh->ah_sn);
      strcat(strMd,strAH_SPI);
    char *strAH_Len=ConvertToString(ahh->ahl);
      strcat(strMd,strAH_Len);

    MDString(strMd,ahh->ah_ad);
     //printf("%s\n",ahh->ah_ad );          




    //IP_HDRINCL to tell the kernel that headers are included in the packet
    int one = 1;
    const int *val = &one;
     
    if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0)
    {
        perror("Error setting IP_HDRINCL");
        exit(0);
    }
     
   
    int y=0;
   while (1)
    {
        //Send the packet
        if (sendto (s, datagram, iph->tot_len ,  0, (struct sockaddr *) &sin, sizeof (sin)) < 0)
        {
            perror("sendto failed");
        }
        //Data send successfully
        else
        {
            printf ("Packet Send. Length : %d  FLOOD POWER!!!!\n" , iph->tot_len);
           // printf("%s\n",ah_h );
        }
       // y=y+1;
    } 
     */
    return 0;
};